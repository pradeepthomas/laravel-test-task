<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});
Route::get('/fake', 'FakeController@index')->name('index');
Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::group(['middleware' => ['auth']], function($user) {   
    //users
    Route::get('/user', 'UserController@user')->name('user');
    Route::get('/user/getDetails', 'UserController@datatables')->name('datatables');

    //Customers
    Route::get('/customers', 'CustomerController@customer')->name('customer');
    Route::get('/customer/getDetails', 'CustomerController@datatables')->name('datatables');

    //products
    Route::get('/products', 'ProductController@product')->name('product');
    Route::get('/product/getDetails/{id}', 'ProductController@datatables')->name('datatables');

    //Activity
    Route::get('/activity', 'ActivityController@activity')->name('activity');
    Route::get('/activity/getDetails', 'ActivityController@datatables')->name('datatables');

    //Orders
    Route::get('/orders', 'OrderController@order')->name('order');
    Route::get('/order/getDetails', 'OrderController@datatables')->name('datatables');
    Route::get('/order/getOrderDetails/{id}', 'OrderController@getOrderDetails')->name('getOrderDetails');
});
