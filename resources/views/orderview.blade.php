@extends('layouts.app')
@section('content')
@section('pageTitle', 'User')
	
<section class="content-header">
	<h1>Orders</h1>
</section>
<table id="example" class="table table-striped table-bordered" style="width:100%">
        <thead>
            <tr>
                <th>Invoice ID </th>
                <th>Customer Name </th>
                <th>Total Amount </th>
                <th>Status</th>  
                <th>Action</th>              
            </tr>
        </thead>        
    </table>

<script>
$(document).ready(function() {
    getOrderdetails();
} );
<!-- get order list -->
function getOrderdetails(){ 
	var URL=$('#base_url').val();
    var usersTable = $('#example').dataTable({
    ajax: URL+"/order/getDetails",
        columns: [
            {data: 'invoice_number'},
            {data: 'name'},
            {data: 'total_amount'},
            {data: 'status'},
            {data: 'id'},
        ],         
        'columnDefs': [{ 
            "targets": -1,            
            'render': function(data, type, full, meta) {
               var id =data;                  
               return '<a target="_blank" href="'+URL+'/order/getOrderDetails/'+data+'">View</a>';
            }
        }]              
	});	
}

</script>

@endsection