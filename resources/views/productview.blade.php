@extends('layouts.app')
@section('content')
@section('pageTitle', 'User')
	
<section class="content-header">
	<h1>Products</h1>
</section>
<style>
.setdrop{
	width: 150px;
	position: absolute;
	right: calc(100% - 70%);
	top: calc(100% - 82%);
    z-index: 100000;
}
</style>
<!-- Filter to narrow product list based on stock. -->
<div class="form-group ">
	<label for="exampleFormControlSelect1">Example select</label>
	<select class="form-control setdrop" id="exampleFormControlSelect1">
	  <option value="1">Both</option>
	  <option value="2">In stock</option>
	  <option value="3">Out of stock</option>	  
	</select>
</div>
<table id="example" class="table table-striped table-bordered" style="width:100%">
	<thead>
		<tr>
			<th>Product Name</th>
			<th>Price</th>                
			<th>Stock</th>                
		</tr>
	</thead>
</table>

<script>
$(document).ready(function() {
    getuserdetails(1);
	$('body').on('change','#exampleFormControlSelect1',function(){
		var val=$('#exampleFormControlSelect1').val();
		var table =$('#example').DataTable();
		table.destroy();
		getuserdetails(val);
	})
} );

function getuserdetails(id){ // get user record
	var URL=$('#base_url').val();
    var usersTable = $('#example').dataTable({
    ajax: URL+"/product/getDetails/"+id,
        columns: [
            {data: 'name'},
            {data: 'price'},
            {data: 'in_stock'}
        ]        
	});	
}

</script>
@endsection