@extends('layouts.app') @section('content') @section('pageTitle', 'User')

<section class="content-header">
    <h1>Order Details</h1>
</section>
<style>
	table {
		font-family: arial, sans-serif;
		border-collapse: collapse;
		width: 70%;
	}
	
	td,th{
		border: 1px solid #dddddd;		
		padding: 8px;
		width: 100px;
	}
	.table_two tr{
		background-color: #dddddd;
	}
	.order_name_lbl{
		position: relative;
		font-size: 18px;
		top: 36px;
		left: calc(100% - 130%);
	}
	.customer_name_lbl{		
		position: relative;
		font-size: 18px;
		right: -24%;
	}	
</style>
<div class="row">
	<div class="col-md-12">
		<div class="">
			<label class="order_name_lbl"><b>Invoice ID:</b>{{isset($order_details[0]) ? $order_details[0]->invoice_number : '' }}</label>
		</div>
		<div class="">
			<label class="customer_name_lbl"><b>Customer name:</b>{{isset($order_details[0]) ? $order_details[0]->customer_name : '' }}</label>	
		</div>
	</div>
</div>

<table>
	<tr>		
		<th>Product name</th>            
		<th>Price</th>
	</tr>
	@foreach($order_details as $row )
	<tr>            		
		<td>{{ $row->products_name }}</td>
		<td>{{ $row->price }}</td>			
	</tr>
	@endforeach	
</table>
<table class="table_two">	
	<tr>            		
		<td>Total</td>
		<td>{{isset($order_details[0]) ? $order_details[0]->total_amount : '' }}</td>			
	</tr>
</table>
@endsection