@extends('layouts.app')
@section('content')
@section('pageTitle', 'Customer')
	
<section class="content-header">
	<h1>Users</h1>
</section>
<table id="example" class="table table-striped table-bordered" style="width:100%">
        <thead>
            <tr>
                <th>Name</th>
                <th>Email</th> 
                <th>Registered On</th>                   
            </tr>
        </thead>        
    </table>

<script>
$(document).ready(function() {
    getUserDetails();
} );
<!-- get Customer List -->
function getUserDetails(){ 
	var URL=$('#base_url').val();
    var usersTable = $('#example').dataTable({
    ajax: URL+"/user/getDetails",
        columns: [
            {data: 'name'},
            {data: 'email'},
            {data:'created_at'}
        ],
		'columnDefs': [{ 
            "targets": -1,            
            'render': function(data, type, full, meta) {               				
				var today = new Date(data);
				var dd = today.getDate();
				var mm = today.getMonth() + 1; //January is 0!

				var yyyy = today.getFullYear();
				if (dd < 10) {
				  dd = '0' + dd;
				} 
				if (mm < 10) {
				  mm = '0' + mm;
				} 
				var today = dd + '/' + mm + '/' + yyyy;				
				return today;
            }
        }] 		
	});	
}

</script>

@endsection