@extends('layouts.app')
@section('content')
@section('pageTitle', 'Activity')
	
<section class="content-header">
	<h1>Activity Log</h1>
</section>
<table id="example" class="table table-striped table-bordered" style="width:100%">
        <thead>
            <tr>                
                <th>Description</th>                
            </tr>
        </thead>        
    </table>

<script>
$(document).ready(function() {
    getActivityDetails();
} );
<!-- get Activity Log List -->
function getActivityDetails(){
	var URL=$('#base_url').val();
    var usersTable = $('#example').dataTable({
    ajax: URL+"/activity/getDetails",
        columns: [            
            {data: 'description'}
        ]        
	});	
}
</script>
@endsection