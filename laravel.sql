-- phpMyAdmin SQL Dump
-- version 4.7.9
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3306
-- Generation Time: Jun 22, 2019 at 02:24 PM
-- Server version: 5.7.21
-- PHP Version: 7.2.4

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `laravel`
--

-- --------------------------------------------------------

--
-- Table structure for table `abilities`
--

DROP TABLE IF EXISTS `abilities`;
CREATE TABLE IF NOT EXISTS `abilities` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `entity_id` int(10) UNSIGNED DEFAULT NULL,
  `entity_type` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `only_owned` tinyint(1) NOT NULL DEFAULT '0',
  `options` json DEFAULT NULL,
  `scope` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `abilities_scope_index` (`scope`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `activity_log`
--

DROP TABLE IF EXISTS `activity_log`;
CREATE TABLE IF NOT EXISTS `activity_log` (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `log_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `subject_id` bigint(20) UNSIGNED DEFAULT NULL,
  `subject_type` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `causer_id` bigint(20) UNSIGNED DEFAULT NULL,
  `causer_type` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `properties` json DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `activity_log_log_name_index` (`log_name`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `assigned_roles`
--

DROP TABLE IF EXISTS `assigned_roles`;
CREATE TABLE IF NOT EXISTS `assigned_roles` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `role_id` int(10) UNSIGNED NOT NULL,
  `entity_id` int(10) UNSIGNED NOT NULL,
  `entity_type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `restricted_to_id` int(10) UNSIGNED DEFAULT NULL,
  `restricted_to_type` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `scope` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `assigned_roles_entity_index` (`entity_id`,`entity_type`,`scope`),
  KEY `assigned_roles_role_id_index` (`role_id`),
  KEY `assigned_roles_scope_index` (`scope`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `assigned_roles`
--

INSERT INTO `assigned_roles` (`id`, `role_id`, `entity_id`, `entity_type`, `restricted_to_id`, `restricted_to_type`, `scope`) VALUES
(1, 1, 1, 'App\\User', NULL, NULL, NULL),
(2, 2, 2, 'App\\User', NULL, NULL, NULL),
(3, 3, 3, 'App\\User', NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `customers`
--

DROP TABLE IF EXISTS `customers`;
CREATE TABLE IF NOT EXISTS `customers` (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `customers_email_unique` (`email`)
) ENGINE=MyISAM AUTO_INCREMENT=201 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `customers`
--

INSERT INTO `customers` (`id`, `name`, `email`, `created_at`, `updated_at`) VALUES
(1, 'Georgette Leannon', 'grover.dare@gmail.com', '2019-06-22 05:29:50', '2019-06-22 05:29:50'),
(2, 'Wallace Schuppe MD', 'jaskolski.janessa@spinka.com', '2019-06-22 05:29:50', '2019-06-22 05:29:50'),
(3, 'Ali Sawayn', 'lon96@gmail.com', '2019-06-22 05:29:50', '2019-06-22 05:29:50'),
(4, 'Ulises Sipes', 'gleichner.stella@hotmail.com', '2019-06-22 05:29:50', '2019-06-22 05:29:50'),
(5, 'Prof. Elmer O\'Hara', 'ferry.glenda@hotmail.com', '2019-06-22 05:29:50', '2019-06-22 05:29:50'),
(6, 'Ms. Alessia Olson III', 'amayert@gmail.com', '2019-06-22 05:29:50', '2019-06-22 05:29:50'),
(7, 'Ms. Annabell Zboncak DVM', 'kutch.reva@mcclure.org', '2019-06-22 05:29:50', '2019-06-22 05:29:50'),
(8, 'Darron Jacobs', 'derek80@gmail.com', '2019-06-22 05:29:50', '2019-06-22 05:29:50'),
(9, 'Elwyn Dare', 'conn.kyleigh@gmail.com', '2019-06-22 05:29:50', '2019-06-22 05:29:50'),
(10, 'Antonina Windler', 'bsporer@hotmail.com', '2019-06-22 05:29:50', '2019-06-22 05:29:50'),
(11, 'Alfredo Hackett', 'balistreri.albina@yahoo.com', '2019-06-22 05:29:50', '2019-06-22 05:29:50'),
(12, 'Stacey Bauch', 'corkery.irma@ohara.com', '2019-06-22 05:29:50', '2019-06-22 05:29:50'),
(13, 'Barbara Keeling', 'lang.novella@gmail.com', '2019-06-22 05:29:50', '2019-06-22 05:29:50'),
(14, 'Rocky Reichel', 'noah04@lehner.com', '2019-06-22 05:29:50', '2019-06-22 05:29:50'),
(15, 'Dr. Shawna Paucek DVM', 'ella.homenick@cormier.biz', '2019-06-22 05:29:50', '2019-06-22 05:29:50'),
(16, 'Lambert Von', 'cale48@yahoo.com', '2019-06-22 05:29:50', '2019-06-22 05:29:50'),
(17, 'Emelie Willms DDS', 'jeremie72@yahoo.com', '2019-06-22 05:29:50', '2019-06-22 05:29:50'),
(18, 'Demond Predovic Sr.', 'jbogisich@yahoo.com', '2019-06-22 05:29:50', '2019-06-22 05:29:50'),
(19, 'Kasey Lynch', 'horace.hartmann@hotmail.com', '2019-06-22 05:29:50', '2019-06-22 05:29:50'),
(20, 'Amparo Medhurst', 'beahan.ana@yahoo.com', '2019-06-22 05:29:50', '2019-06-22 05:29:50'),
(21, 'Adonis D\'Amore DDS', 'sreichel@hotmail.com', '2019-06-22 05:29:50', '2019-06-22 05:29:50'),
(22, 'Prof. Queen Yost III', 'wolf.elvis@hotmail.com', '2019-06-22 05:29:50', '2019-06-22 05:29:50'),
(23, 'Prof. Lavern Durgan', 'rosemarie.schaden@rogahn.com', '2019-06-22 05:29:50', '2019-06-22 05:29:50'),
(24, 'Miss Hattie Reilly III', 'margaret88@gmail.com', '2019-06-22 05:29:50', '2019-06-22 05:29:50'),
(25, 'Leonor Klocko', 'novella87@dare.com', '2019-06-22 05:29:50', '2019-06-22 05:29:50'),
(26, 'Viviane Schulist', 'jwhite@lang.com', '2019-06-22 05:29:50', '2019-06-22 05:29:50'),
(27, 'Lenny Luettgen IV', 'stiedemann.harmon@greenholt.com', '2019-06-22 05:29:50', '2019-06-22 05:29:50'),
(28, 'Patience Cummings', 'brock.batz@yahoo.com', '2019-06-22 05:29:50', '2019-06-22 05:29:50'),
(29, 'Amparo Weimann', 'ansel.balistreri@wisoky.com', '2019-06-22 05:29:50', '2019-06-22 05:29:50'),
(30, 'Chyna Kuhlman MD', 'ullrich.jerald@cummings.com', '2019-06-22 05:29:50', '2019-06-22 05:29:50'),
(31, 'Maudie Heathcote MD', 'fern44@upton.biz', '2019-06-22 05:29:50', '2019-06-22 05:29:50'),
(32, 'Joyce Reichel', 'lstroman@stehr.biz', '2019-06-22 05:29:50', '2019-06-22 05:29:50'),
(33, 'Stewart Lubowitz', 'kgreenfelder@gmail.com', '2019-06-22 05:29:50', '2019-06-22 05:29:50'),
(34, 'Greyson Stokes', 'zora.turner@conroy.com', '2019-06-22 05:29:50', '2019-06-22 05:29:50'),
(35, 'Odell Hand', 'hammes.foster@keebler.org', '2019-06-22 05:29:50', '2019-06-22 05:29:50'),
(36, 'Prof. Martin Stehr', 'jace25@gmail.com', '2019-06-22 05:29:50', '2019-06-22 05:29:50'),
(37, 'Christop Schuster', 'joesph44@gmail.com', '2019-06-22 05:29:50', '2019-06-22 05:29:50'),
(38, 'Deven Sawayn', 'denesik.rachelle@hotmail.com', '2019-06-22 05:29:50', '2019-06-22 05:29:50'),
(39, 'Talia Jast', 'west.austyn@hartmann.info', '2019-06-22 05:29:50', '2019-06-22 05:29:50'),
(40, 'Susana Hand', 'tromp.shawn@yahoo.com', '2019-06-22 05:29:50', '2019-06-22 05:29:50'),
(41, 'Lucinda Rolfson', 'jlind@gmail.com', '2019-06-22 05:29:50', '2019-06-22 05:29:50'),
(42, 'Cale Vandervort', 'ziemann.keeley@gmail.com', '2019-06-22 05:29:50', '2019-06-22 05:29:50'),
(43, 'Jerrell Gleason', 'natalie.greenholt@yahoo.com', '2019-06-22 05:29:50', '2019-06-22 05:29:50'),
(44, 'Marlen Gusikowski', 'edyth24@wyman.info', '2019-06-22 05:29:50', '2019-06-22 05:29:50'),
(45, 'Shanna Herzog PhD', 'kristian.bednar@gmail.com', '2019-06-22 05:29:50', '2019-06-22 05:29:50'),
(46, 'Prof. Charley O\'Kon DVM', 'veum.julius@gmail.com', '2019-06-22 05:29:50', '2019-06-22 05:29:50'),
(47, 'Willow Gottlieb', 'xreilly@pfeffer.info', '2019-06-22 05:29:50', '2019-06-22 05:29:50'),
(48, 'Miss Mossie Braun PhD', 'kunde.rodrigo@morar.com', '2019-06-22 05:29:50', '2019-06-22 05:29:50'),
(49, 'Miss Audreanne Corwin PhD', 'miracle38@heidenreich.net', '2019-06-22 05:29:50', '2019-06-22 05:29:50'),
(50, 'Austyn Stamm', 'annabell.oreilly@gmail.com', '2019-06-22 05:29:50', '2019-06-22 05:29:50'),
(51, 'Luther Zboncak', 'tromp.mason@johnston.com', '2019-06-22 05:29:50', '2019-06-22 05:29:50'),
(52, 'Ms. Baby Haag II', 'howell.scotty@yahoo.com', '2019-06-22 05:29:50', '2019-06-22 05:29:50'),
(53, 'Prof. Daphney Leffler V', 'gussie79@wolff.com', '2019-06-22 05:29:50', '2019-06-22 05:29:50'),
(54, 'Ike Doyle', 'mayer.paris@funk.com', '2019-06-22 05:29:50', '2019-06-22 05:29:50'),
(55, 'Aron Hauck', 'beer.margarette@yahoo.com', '2019-06-22 05:29:50', '2019-06-22 05:29:50'),
(56, 'Robin Kunde', 'wintheiser.winifred@yahoo.com', '2019-06-22 05:29:50', '2019-06-22 05:29:50'),
(57, 'Shaina Durgan', 'donnelly.dustin@hotmail.com', '2019-06-22 05:29:50', '2019-06-22 05:29:50'),
(58, 'Austin Gottlieb', 'hane.juston@gorczany.net', '2019-06-22 05:29:50', '2019-06-22 05:29:50'),
(59, 'Mr. Quentin Nicolas', 'amiller@yahoo.com', '2019-06-22 05:29:50', '2019-06-22 05:29:50'),
(60, 'Heloise Pouros Jr.', 'ksteuber@stamm.info', '2019-06-22 05:29:50', '2019-06-22 05:29:50'),
(61, 'Ethel Block', 'danny56@gmail.com', '2019-06-22 05:29:50', '2019-06-22 05:29:50'),
(62, 'Stuart Wiza', 'kshlerin.river@schuster.com', '2019-06-22 05:29:50', '2019-06-22 05:29:50'),
(63, 'Mable Barton', 'shields.zola@christiansen.net', '2019-06-22 05:29:50', '2019-06-22 05:29:50'),
(64, 'Maida Schulist PhD', 'megane58@yahoo.com', '2019-06-22 05:29:50', '2019-06-22 05:29:50'),
(65, 'Cole Padberg', 'oschamberger@gerlach.com', '2019-06-22 05:29:50', '2019-06-22 05:29:50'),
(66, 'Prof. Hoyt Strosin PhD', 'gkiehn@gerlach.com', '2019-06-22 05:29:50', '2019-06-22 05:29:50'),
(67, 'Dr. Orion Green', 'jessie70@yahoo.com', '2019-06-22 05:29:50', '2019-06-22 05:29:50'),
(68, 'Rosella Schultz', 'hoppe.mekhi@oconner.biz', '2019-06-22 05:29:50', '2019-06-22 05:29:50'),
(69, 'Melisa Swift', 'spencer.caroline@mayert.org', '2019-06-22 05:29:50', '2019-06-22 05:29:50'),
(70, 'Idella Stark', 'jasmin13@hotmail.com', '2019-06-22 05:29:50', '2019-06-22 05:29:50'),
(71, 'Kirsten Rohan', 'tony75@hotmail.com', '2019-06-22 05:29:50', '2019-06-22 05:29:50'),
(72, 'Crystel Wunsch', 'wyatt99@gmail.com', '2019-06-22 05:29:50', '2019-06-22 05:29:50'),
(73, 'Brad Aufderhar', 'elisabeth51@gmail.com', '2019-06-22 05:29:50', '2019-06-22 05:29:50'),
(74, 'Chaz Mraz', 'rosanna.beer@rosenbaum.com', '2019-06-22 05:29:50', '2019-06-22 05:29:50'),
(75, 'Prof. Kennith O\'Kon', 'pasquale10@yahoo.com', '2019-06-22 05:29:50', '2019-06-22 05:29:50'),
(76, 'Miss Verona Casper', 'ruthe.padberg@donnelly.org', '2019-06-22 05:29:50', '2019-06-22 05:29:50'),
(77, 'Emma Raynor', 'ustrosin@hotmail.com', '2019-06-22 05:29:50', '2019-06-22 05:29:50'),
(78, 'Jaiden Roob', 'jalen.terry@gmail.com', '2019-06-22 05:29:50', '2019-06-22 05:29:50'),
(79, 'Beatrice Waters', 'deanna.bogan@eichmann.org', '2019-06-22 05:29:50', '2019-06-22 05:29:50'),
(80, 'Ahmed Yundt', 'tcrist@yahoo.com', '2019-06-22 05:29:50', '2019-06-22 05:29:50'),
(81, 'Grayce Bauch', 'consuelo45@reichert.info', '2019-06-22 05:29:50', '2019-06-22 05:29:50'),
(82, 'Dr. Ottis Bergstrom', 'bins.lula@jacobson.com', '2019-06-22 05:29:50', '2019-06-22 05:29:50'),
(83, 'Nicola Osinski I', 'rosa.buckridge@herzog.com', '2019-06-22 05:29:50', '2019-06-22 05:29:50'),
(84, 'Timothy Sawayn', 'sophia.heidenreich@hotmail.com', '2019-06-22 05:29:50', '2019-06-22 05:29:50'),
(85, 'Raven Witting', 'clay.turner@yahoo.com', '2019-06-22 05:29:50', '2019-06-22 05:29:50'),
(86, 'Geovanni Cassin', 'mayert.salvatore@miller.biz', '2019-06-22 05:29:50', '2019-06-22 05:29:50'),
(87, 'Zakary Olson', 'veda79@hotmail.com', '2019-06-22 05:29:50', '2019-06-22 05:29:50'),
(88, 'Maegan Wisozk Sr.', 'mosciski.miguel@hotmail.com', '2019-06-22 05:29:50', '2019-06-22 05:29:50'),
(89, 'Floy Thompson', 'meagan11@gutkowski.com', '2019-06-22 05:29:50', '2019-06-22 05:29:50'),
(90, 'Arlie Muller', 'donnelly.river@yahoo.com', '2019-06-22 05:29:50', '2019-06-22 05:29:50'),
(91, 'Earl Altenwerth', 'weston.mertz@yahoo.com', '2019-06-22 05:29:50', '2019-06-22 05:29:50'),
(92, 'Prof. Louie Robel', 'gibson.adella@yahoo.com', '2019-06-22 05:29:50', '2019-06-22 05:29:50'),
(93, 'Mr. Walker Jones MD', 'sedrick33@gmail.com', '2019-06-22 05:29:50', '2019-06-22 05:29:50'),
(94, 'Murray Funk', 'rflatley@yahoo.com', '2019-06-22 05:29:50', '2019-06-22 05:29:50'),
(95, 'Mrs. Brandyn DuBuque DDS', 'lexi92@cassin.com', '2019-06-22 05:29:50', '2019-06-22 05:29:50'),
(96, 'Mr. Ignatius Walter', 'janet20@schulist.com', '2019-06-22 05:29:50', '2019-06-22 05:29:50'),
(97, 'Leif Schultz', 'rau.susana@hotmail.com', '2019-06-22 05:29:50', '2019-06-22 05:29:50'),
(98, 'Prof. Stone Jenkins III', 'delores.carroll@yahoo.com', '2019-06-22 05:29:50', '2019-06-22 05:29:50'),
(99, 'Rhett Grady', 'cartwright.daniella@bergstrom.com', '2019-06-22 05:29:50', '2019-06-22 05:29:50'),
(100, 'Felix Rempel', 'beatty.royce@yahoo.com', '2019-06-22 05:29:50', '2019-06-22 05:29:50'),
(101, 'Vanessa Bernier', 'upurdy@gmail.com', '2019-06-22 05:29:50', '2019-06-22 05:29:50'),
(102, 'Prof. Clint Flatley I', 'annabel76@terry.com', '2019-06-22 05:29:50', '2019-06-22 05:29:50'),
(103, 'Ms. Ethyl Schultz IV', 'brekke.mohammad@zboncak.com', '2019-06-22 05:29:50', '2019-06-22 05:29:50'),
(104, 'Wendy King', 'rowe.vanessa@koss.com', '2019-06-22 05:29:50', '2019-06-22 05:29:50'),
(105, 'Hayden Larkin', 'konopelski.miguel@yahoo.com', '2019-06-22 05:29:50', '2019-06-22 05:29:50'),
(106, 'Mrs. Shaniya Gerhold', 'rory14@hotmail.com', '2019-06-22 05:29:50', '2019-06-22 05:29:50'),
(107, 'Elwin Walker', 'kozey.ophelia@sanford.com', '2019-06-22 05:29:50', '2019-06-22 05:29:50'),
(108, 'Marina Waters', 'tbode@yahoo.com', '2019-06-22 05:29:50', '2019-06-22 05:29:50'),
(109, 'Rosina Daniel', 'von.kaelyn@yahoo.com', '2019-06-22 05:29:50', '2019-06-22 05:29:50'),
(110, 'Mr. Nasir Schmitt', 'ibergnaum@gmail.com', '2019-06-22 05:29:50', '2019-06-22 05:29:50'),
(111, 'Eulalia Kautzer', 'oconnell.valerie@hammes.org', '2019-06-22 05:29:50', '2019-06-22 05:29:50'),
(112, 'Glenda O\'Connell', 'brandy.lockman@yahoo.com', '2019-06-22 05:29:50', '2019-06-22 05:29:50'),
(113, 'Rusty Langworth', 'jevon95@yahoo.com', '2019-06-22 05:29:50', '2019-06-22 05:29:50'),
(114, 'Leon Steuber', 'zkoepp@hotmail.com', '2019-06-22 05:29:50', '2019-06-22 05:29:50'),
(115, 'Prof. Daryl Gulgowski I', 'chelsie34@gmail.com', '2019-06-22 05:29:50', '2019-06-22 05:29:50'),
(116, 'Abdul Zboncak PhD', 'pouros.shanna@sauer.org', '2019-06-22 05:29:50', '2019-06-22 05:29:50'),
(117, 'Lurline Beatty', 'hayley.purdy@gmail.com', '2019-06-22 05:29:50', '2019-06-22 05:29:50'),
(118, 'Lucious Schneider', 'joshuah.muller@hotmail.com', '2019-06-22 05:29:50', '2019-06-22 05:29:50'),
(119, 'Julian Schumm', 'maggie32@hotmail.com', '2019-06-22 05:29:50', '2019-06-22 05:29:50'),
(120, 'Junior Bogan DDS', 'megane.schoen@yahoo.com', '2019-06-22 05:29:50', '2019-06-22 05:29:50'),
(121, 'Mr. Julio Hauck', 'lorenz29@hotmail.com', '2019-06-22 05:29:50', '2019-06-22 05:29:50'),
(122, 'Jaylan Bradtke', 'lstracke@gmail.com', '2019-06-22 05:29:50', '2019-06-22 05:29:50'),
(123, 'Prof. Jacinto Shanahan', 'weimann.clementina@gmail.com', '2019-06-22 05:29:50', '2019-06-22 05:29:50'),
(124, 'Dr. Kip Lehner', 'howell.nyasia@vandervort.com', '2019-06-22 05:29:50', '2019-06-22 05:29:50'),
(125, 'Miguel Lehner', 'weber.jamel@doyle.com', '2019-06-22 05:29:50', '2019-06-22 05:29:50'),
(126, 'Eulalia Roob IV', 'ezboncak@gmail.com', '2019-06-22 05:29:50', '2019-06-22 05:29:50'),
(127, 'Prof. Jeanie Block Jr.', 'murphy.romaine@terry.com', '2019-06-22 05:29:50', '2019-06-22 05:29:50'),
(128, 'Dr. Neal Beer DDS', 'retha32@orn.com', '2019-06-22 05:29:50', '2019-06-22 05:29:50'),
(129, 'Zander Reinger', 'ayost@hotmail.com', '2019-06-22 05:29:50', '2019-06-22 05:29:50'),
(130, 'Gladyce Homenick', 'rowland99@hotmail.com', '2019-06-22 05:29:50', '2019-06-22 05:29:50'),
(131, 'Marco Gerhold', 'dessie.rohan@hotmail.com', '2019-06-22 05:29:50', '2019-06-22 05:29:50'),
(132, 'Ronny Schiller I', 'kirsten.schumm@skiles.com', '2019-06-22 05:29:50', '2019-06-22 05:29:50'),
(133, 'Solon Goyette', 'pete.blanda@hotmail.com', '2019-06-22 05:29:50', '2019-06-22 05:29:50'),
(134, 'Mr. Van Grant MD', 'jchristiansen@botsford.com', '2019-06-22 05:29:50', '2019-06-22 05:29:50'),
(135, 'Dr. Hank Kautzer', 'gjakubowski@yahoo.com', '2019-06-22 05:29:50', '2019-06-22 05:29:50'),
(136, 'Benny Satterfield', 'schumm.mitchel@armstrong.com', '2019-06-22 05:29:50', '2019-06-22 05:29:50'),
(137, 'Alexa Bayer IV', 'dkeebler@gmail.com', '2019-06-22 05:29:50', '2019-06-22 05:29:50'),
(138, 'Arnold Berge', 'jaida53@hotmail.com', '2019-06-22 05:29:50', '2019-06-22 05:29:50'),
(139, 'Golden Berge', 'erik.torp@oreilly.com', '2019-06-22 05:29:50', '2019-06-22 05:29:50'),
(140, 'Mitchell Kuhlman', 'danyka.breitenberg@yahoo.com', '2019-06-22 05:29:50', '2019-06-22 05:29:50'),
(141, 'Mrs. Alaina Cummerata IV', 'klocko.desiree@gmail.com', '2019-06-22 05:29:50', '2019-06-22 05:29:50'),
(142, 'Edythe Gislason', 'celia80@hotmail.com', '2019-06-22 05:29:50', '2019-06-22 05:29:50'),
(143, 'Macy Jacobs', 'carissa.berge@rath.biz', '2019-06-22 05:29:50', '2019-06-22 05:29:50'),
(144, 'Sallie Wisoky', 'fahey.marvin@hotmail.com', '2019-06-22 05:29:50', '2019-06-22 05:29:50'),
(145, 'Miss Syble Lakin IV', 'sonya.franecki@gmail.com', '2019-06-22 05:29:50', '2019-06-22 05:29:50'),
(146, 'Dr. Gretchen Dooley', 'forn@olson.com', '2019-06-22 05:29:50', '2019-06-22 05:29:50'),
(147, 'Cornelius Ullrich', 'fadel.mateo@marquardt.com', '2019-06-22 05:29:50', '2019-06-22 05:29:50'),
(148, 'Friedrich Walsh', 'dee06@hotmail.com', '2019-06-22 05:29:50', '2019-06-22 05:29:50'),
(149, 'Emmie Pagac', 'xnikolaus@kozey.com', '2019-06-22 05:29:50', '2019-06-22 05:29:50'),
(150, 'Dr. Saige D\'Amore', 'aylin99@schaefer.org', '2019-06-22 05:29:50', '2019-06-22 05:29:50'),
(151, 'Lilly Leannon', 'darwin.grimes@gottlieb.com', '2019-06-22 05:29:50', '2019-06-22 05:29:50'),
(152, 'Willis Bradtke IV', 'mraz.ottilie@yahoo.com', '2019-06-22 05:29:50', '2019-06-22 05:29:50'),
(153, 'Mitchell Stanton', 'uleannon@okon.com', '2019-06-22 05:29:50', '2019-06-22 05:29:50'),
(154, 'Ms. Rachelle Cartwright', 'clark31@marquardt.info', '2019-06-22 05:29:50', '2019-06-22 05:29:50'),
(155, 'Dewayne Nitzsche Jr.', 'corkery.bella@gmail.com', '2019-06-22 05:29:50', '2019-06-22 05:29:50'),
(156, 'Margarete Hane', 'tavares73@rohan.com', '2019-06-22 05:29:50', '2019-06-22 05:29:50'),
(157, 'Greyson Kunde', 'grimes.esther@yahoo.com', '2019-06-22 05:29:50', '2019-06-22 05:29:50'),
(158, 'Loren Collins', 'geo.von@yahoo.com', '2019-06-22 05:29:50', '2019-06-22 05:29:50'),
(159, 'Reba McDermott', 'vwuckert@bins.org', '2019-06-22 05:29:50', '2019-06-22 05:29:50'),
(160, 'Mr. Rudy Bailey IV', 'fabian77@gmail.com', '2019-06-22 05:29:50', '2019-06-22 05:29:50'),
(161, 'Freda Larkin', 'fwiza@yahoo.com', '2019-06-22 05:29:50', '2019-06-22 05:29:50'),
(162, 'Claud Orn', 'erik69@morissette.com', '2019-06-22 05:29:50', '2019-06-22 05:29:50'),
(163, 'Mr. Vincenzo Reynolds V', 'lauryn97@yahoo.com', '2019-06-22 05:29:50', '2019-06-22 05:29:50'),
(164, 'Jodie Cremin', 'kilback.nia@gmail.com', '2019-06-22 05:29:50', '2019-06-22 05:29:50'),
(165, 'Dr. Christiana Dietrich', 'clark.cremin@baumbach.biz', '2019-06-22 05:29:50', '2019-06-22 05:29:50'),
(166, 'Davonte Conroy', 'hettinger.garth@yahoo.com', '2019-06-22 05:29:50', '2019-06-22 05:29:50'),
(167, 'Manuela Conn', 'elsa19@yahoo.com', '2019-06-22 05:29:50', '2019-06-22 05:29:50'),
(168, 'Mrs. Meaghan Renner IV', 'tkonopelski@gmail.com', '2019-06-22 05:29:50', '2019-06-22 05:29:50'),
(169, 'Jeremy Howell', 'dwunsch@hotmail.com', '2019-06-22 05:29:50', '2019-06-22 05:29:50'),
(170, 'Isabelle Tromp', 'lrutherford@pacocha.com', '2019-06-22 05:29:50', '2019-06-22 05:29:50'),
(171, 'Effie Will', 'friesen.cody@yahoo.com', '2019-06-22 05:29:50', '2019-06-22 05:29:50'),
(172, 'Beverly Feil', 'howe.imelda@yahoo.com', '2019-06-22 05:29:50', '2019-06-22 05:29:50'),
(173, 'Jana Wyman', 'raleigh.cassin@heller.info', '2019-06-22 05:29:50', '2019-06-22 05:29:50'),
(174, 'Vilma Mertz', 'raina13@yahoo.com', '2019-06-22 05:29:50', '2019-06-22 05:29:50'),
(175, 'Dr. Cale Pagac I', 'runte.abdiel@greenholt.biz', '2019-06-22 05:29:50', '2019-06-22 05:29:50'),
(176, 'Nelle Mills', 'rnikolaus@gmail.com', '2019-06-22 05:29:50', '2019-06-22 05:29:50'),
(177, 'Prof. Schuyler Glover', 'nmiller@schumm.com', '2019-06-22 05:29:50', '2019-06-22 05:29:50'),
(178, 'Emma Pacocha IV', 'kailyn83@yahoo.com', '2019-06-22 05:29:50', '2019-06-22 05:29:50'),
(179, 'Alphonso Christiansen', 'zpouros@yahoo.com', '2019-06-22 05:29:50', '2019-06-22 05:29:50'),
(180, 'Adan Rau', 'donato40@hammes.com', '2019-06-22 05:29:50', '2019-06-22 05:29:50'),
(181, 'Elliott Purdy', 'pfeffer.florine@hintz.biz', '2019-06-22 05:29:50', '2019-06-22 05:29:50'),
(182, 'Prof. Macey Schmeler II', 'javonte65@gmail.com', '2019-06-22 05:29:50', '2019-06-22 05:29:50'),
(183, 'Caden Weber', 'ihayes@prosacco.com', '2019-06-22 05:29:50', '2019-06-22 05:29:50'),
(184, 'Jade Boyer', 'orville40@hotmail.com', '2019-06-22 05:29:50', '2019-06-22 05:29:50'),
(185, 'Gordon Dibbert', 'osinski.bulah@osinski.biz', '2019-06-22 05:29:50', '2019-06-22 05:29:50'),
(186, 'Fernando Lemke Sr.', 'hahn.angelita@gmail.com', '2019-06-22 05:29:50', '2019-06-22 05:29:50'),
(187, 'Delaney Goyette', 'romaguera.erick@ondricka.com', '2019-06-22 05:29:50', '2019-06-22 05:29:50'),
(188, 'Dr. Deanna Sauer', 'stroman.jakayla@johnston.com', '2019-06-22 05:29:50', '2019-06-22 05:29:50'),
(189, 'Felipe Schroeder', 'mkuhic@herzog.org', '2019-06-22 05:29:50', '2019-06-22 05:29:50'),
(190, 'Carter Nader', 'sonya.pouros@gmail.com', '2019-06-22 05:29:50', '2019-06-22 05:29:50'),
(191, 'Lilla King', 'van.wolff@kerluke.com', '2019-06-22 05:29:50', '2019-06-22 05:29:50'),
(192, 'Ora Ruecker', 'sylvia.lehner@kihn.com', '2019-06-22 05:29:50', '2019-06-22 05:29:50'),
(193, 'Zackery Smith', 'beier.tremaine@hotmail.com', '2019-06-22 05:29:50', '2019-06-22 05:29:50'),
(194, 'Llewellyn Haag', 'qkemmer@wehner.org', '2019-06-22 05:29:50', '2019-06-22 05:29:50'),
(195, 'Aylin Lesch', 'zeichmann@langosh.net', '2019-06-22 05:29:50', '2019-06-22 05:29:50'),
(196, 'Joseph Schimmel V', 'oupton@hotmail.com', '2019-06-22 05:29:50', '2019-06-22 05:29:50'),
(197, 'Tristian Brakus', 'gleichner.santina@gmail.com', '2019-06-22 05:29:50', '2019-06-22 05:29:50'),
(198, 'Reba Schaden', 'mmosciski@gorczany.com', '2019-06-22 05:29:50', '2019-06-22 05:29:50'),
(199, 'Raleigh McClure', 'lorenz52@stoltenberg.biz', '2019-06-22 05:29:50', '2019-06-22 05:29:50'),
(200, 'Augustus Koelpin', 'molly.bahringer@stoltenberg.biz', '2019-06-22 05:29:50', '2019-06-22 05:29:50');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

DROP TABLE IF EXISTS `migrations`;
CREATE TABLE IF NOT EXISTS `migrations` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=9 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_customer_table', 1),
(2, '2014_10_12_000000_create_users_table', 1),
(3, '2014_10_12_100000_create_password_resets_table', 1),
(4, '2019_06_20_112910_create_order_table', 1),
(5, '2019_06_20_123402_create_products_table', 1),
(6, '2019_06_20_124139_create__order_items_table', 1),
(7, '2019_06_22_100129_create_activity_log_table', 1),
(8, '2019_06_22_122005_create_bouncer_tables', 2);

-- --------------------------------------------------------

--
-- Table structure for table `orders`
--

DROP TABLE IF EXISTS `orders`;
CREATE TABLE IF NOT EXISTS `orders` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `invoice_number` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `total_amount` decimal(5,2) NOT NULL,
  `status` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `customer_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=51 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `orders`
--

INSERT INTO `orders` (`id`, `invoice_number`, `total_amount`, `status`, `customer_id`, `created_at`, `updated_at`) VALUES
(1, '0001', '85.00', 'new', 149, '2019-06-22 08:50:44', '2019-06-22 08:50:44'),
(2, '0002', '18.00', 'processed', 112, '2019-06-22 08:50:44', '2019-06-22 08:50:44'),
(3, '0003', '116.00', 'processed', 169, '2019-06-22 08:50:44', '2019-06-22 08:50:44'),
(4, '0004', '107.00', 'new', 51, '2019-06-22 08:50:44', '2019-06-22 08:50:44'),
(5, '0005', '163.00', 'new', 106, '2019-06-22 08:50:44', '2019-06-22 08:50:44'),
(6, '0006', '132.00', 'processed', 184, '2019-06-22 08:50:44', '2019-06-22 08:50:44'),
(7, '0007', '171.00', 'new', 109, '2019-06-22 08:50:44', '2019-06-22 08:50:44'),
(8, '0008', '164.00', 'processed', 98, '2019-06-22 08:50:44', '2019-06-22 08:50:44'),
(9, '0009', '140.00', 'processed', 43, '2019-06-22 08:50:44', '2019-06-22 08:50:44'),
(10, '00010', '55.00', 'new', 82, '2019-06-22 08:50:44', '2019-06-22 08:50:44'),
(11, '00011', '63.00', 'new', 38, '2019-06-22 08:50:44', '2019-06-22 08:50:44'),
(12, '00012', '113.00', 'new', 74, '2019-06-22 08:50:44', '2019-06-22 08:50:44'),
(13, '00013', '82.00', 'new', 85, '2019-06-22 08:50:44', '2019-06-22 08:50:44'),
(14, '00014', '134.00', 'new', 194, '2019-06-22 08:50:44', '2019-06-22 08:50:44'),
(15, '00015', '107.00', 'processed', 166, '2019-06-22 08:50:44', '2019-06-22 08:50:44'),
(16, '00016', '123.00', 'new', 103, '2019-06-22 08:50:44', '2019-06-22 08:50:44'),
(17, '00017', '84.00', 'processed', 33, '2019-06-22 08:50:44', '2019-06-22 08:50:44'),
(18, '00018', '137.00', 'processed', 192, '2019-06-22 08:50:44', '2019-06-22 08:50:44'),
(19, '00019', '110.00', 'new', 123, '2019-06-22 08:50:44', '2019-06-22 08:50:44'),
(20, '00020', '156.00', 'processed', 26, '2019-06-22 08:50:44', '2019-06-22 08:50:44'),
(21, '00021', '97.00', 'new', 136, '2019-06-22 08:50:44', '2019-06-22 08:50:44'),
(22, '00022', '85.00', 'new', 111, '2019-06-22 08:50:44', '2019-06-22 08:50:44'),
(23, '00023', '90.00', 'new', 135, '2019-06-22 08:50:44', '2019-06-22 08:50:44'),
(24, '00024', '94.00', 'processed', 25, '2019-06-22 08:50:44', '2019-06-22 08:50:44'),
(25, '00025', '156.00', 'new', 174, '2019-06-22 08:50:44', '2019-06-22 08:50:44'),
(26, '00026', '77.00', 'processed', 26, '2019-06-22 08:50:44', '2019-06-22 08:50:44'),
(27, '00027', '161.00', 'processed', 177, '2019-06-22 08:50:44', '2019-06-22 08:50:44'),
(28, '00028', '134.00', 'new', 126, '2019-06-22 08:50:44', '2019-06-22 08:50:44'),
(29, '00029', '105.00', 'processed', 26, '2019-06-22 08:50:44', '2019-06-22 08:50:44'),
(30, '00030', '76.00', 'processed', 129, '2019-06-22 08:50:44', '2019-06-22 08:50:44'),
(31, '00031', '120.00', 'processed', 80, '2019-06-22 08:50:44', '2019-06-22 08:50:44'),
(32, '00032', '71.00', 'processed', 52, '2019-06-22 08:50:44', '2019-06-22 08:50:44'),
(33, '00033', '36.00', 'new', 171, '2019-06-22 08:50:44', '2019-06-22 08:50:44'),
(34, '00034', '91.00', 'processed', 98, '2019-06-22 08:50:44', '2019-06-22 08:50:44'),
(35, '00035', '118.00', 'new', 193, '2019-06-22 08:50:44', '2019-06-22 08:50:44'),
(36, '00036', '92.00', 'new', 132, '2019-06-22 08:50:44', '2019-06-22 08:50:44'),
(37, '00037', '89.00', 'processed', 169, '2019-06-22 08:50:44', '2019-06-22 08:50:44'),
(38, '00038', '124.00', 'new', 74, '2019-06-22 08:50:44', '2019-06-22 08:50:44'),
(39, '00039', '84.00', 'new', 166, '2019-06-22 08:50:44', '2019-06-22 08:50:44'),
(40, '00040', '67.00', 'new', 62, '2019-06-22 08:50:44', '2019-06-22 08:50:44'),
(41, '00041', '113.00', 'processed', 137, '2019-06-22 08:50:44', '2019-06-22 08:50:44'),
(42, '00042', '74.00', 'new', 143, '2019-06-22 08:50:44', '2019-06-22 08:50:44'),
(43, '00043', '118.00', 'new', 153, '2019-06-22 08:50:44', '2019-06-22 08:50:44'),
(44, '00044', '125.00', 'processed', 48, '2019-06-22 08:50:44', '2019-06-22 08:50:44'),
(45, '00045', '123.00', 'processed', 53, '2019-06-22 08:50:44', '2019-06-22 08:50:44'),
(46, '00046', '88.00', 'processed', 169, '2019-06-22 08:50:44', '2019-06-22 08:50:44'),
(47, '00047', '155.00', 'processed', 41, '2019-06-22 08:50:44', '2019-06-22 08:50:44'),
(48, '00048', '108.00', 'processed', 88, '2019-06-22 08:50:44', '2019-06-22 08:50:44'),
(49, '00049', '109.00', 'processed', 192, '2019-06-22 08:50:44', '2019-06-22 08:50:44'),
(50, '00050', '137.00', 'new', 22, '2019-06-22 08:50:44', '2019-06-22 08:50:44');

-- --------------------------------------------------------

--
-- Table structure for table `order_items`
--

DROP TABLE IF EXISTS `order_items`;
CREATE TABLE IF NOT EXISTS `order_items` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `order_id` int(10) UNSIGNED NOT NULL,
  `product_id` int(10) UNSIGNED NOT NULL,
  `quantity` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `order_items_order_id_foreign` (`order_id`),
  KEY `order_items_product_id_foreign` (`product_id`)
) ENGINE=MyISAM AUTO_INCREMENT=101 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `order_items`
--

INSERT INTO `order_items` (`id`, `order_id`, `product_id`, `quantity`, `created_at`, `updated_at`) VALUES
(1, 1, 38, 1, '2019-06-22 08:50:44', '2019-06-22 08:50:44'),
(2, 1, 47, 1, '2019-06-22 08:50:44', '2019-06-22 08:50:44'),
(3, 2, 99, 1, '2019-06-22 08:50:44', '2019-06-22 08:50:44'),
(4, 2, 55, 1, '2019-06-22 08:50:44', '2019-06-22 08:50:44'),
(5, 3, 85, 1, '2019-06-22 08:50:44', '2019-06-22 08:50:44'),
(6, 3, 46, 1, '2019-06-22 08:50:44', '2019-06-22 08:50:44'),
(7, 4, 52, 1, '2019-06-22 08:50:44', '2019-06-22 08:50:44'),
(8, 4, 19, 1, '2019-06-22 08:50:44', '2019-06-22 08:50:44'),
(9, 5, 23, 1, '2019-06-22 08:50:44', '2019-06-22 08:50:44'),
(10, 5, 89, 1, '2019-06-22 08:50:44', '2019-06-22 08:50:44'),
(11, 6, 47, 1, '2019-06-22 08:50:44', '2019-06-22 08:50:44'),
(12, 6, 26, 1, '2019-06-22 08:50:44', '2019-06-22 08:50:44'),
(13, 7, 71, 1, '2019-06-22 08:50:44', '2019-06-22 08:50:44'),
(14, 7, 67, 1, '2019-06-22 08:50:44', '2019-06-22 08:50:44'),
(15, 8, 42, 1, '2019-06-22 08:50:44', '2019-06-22 08:50:44'),
(16, 8, 23, 1, '2019-06-22 08:50:44', '2019-06-22 08:50:44'),
(17, 9, 63, 1, '2019-06-22 08:50:44', '2019-06-22 08:50:44'),
(18, 9, 30, 1, '2019-06-22 08:50:44', '2019-06-22 08:50:44'),
(19, 10, 59, 1, '2019-06-22 08:50:44', '2019-06-22 08:50:44'),
(20, 10, 86, 1, '2019-06-22 08:50:44', '2019-06-22 08:50:44'),
(21, 11, 2, 1, '2019-06-22 08:50:44', '2019-06-22 08:50:44'),
(22, 11, 48, 1, '2019-06-22 08:50:44', '2019-06-22 08:50:44'),
(23, 12, 3, 1, '2019-06-22 08:50:44', '2019-06-22 08:50:44'),
(24, 12, 100, 1, '2019-06-22 08:50:44', '2019-06-22 08:50:44'),
(25, 13, 47, 1, '2019-06-22 08:50:44', '2019-06-22 08:50:44'),
(26, 13, 2, 1, '2019-06-22 08:50:44', '2019-06-22 08:50:44'),
(27, 14, 91, 1, '2019-06-22 08:50:44', '2019-06-22 08:50:44'),
(28, 14, 21, 1, '2019-06-22 08:50:44', '2019-06-22 08:50:44'),
(29, 15, 45, 1, '2019-06-22 08:50:44', '2019-06-22 08:50:44'),
(30, 15, 27, 1, '2019-06-22 08:50:44', '2019-06-22 08:50:44'),
(31, 16, 11, 1, '2019-06-22 08:50:44', '2019-06-22 08:50:44'),
(32, 16, 45, 1, '2019-06-22 08:50:44', '2019-06-22 08:50:44'),
(33, 17, 35, 1, '2019-06-22 08:50:44', '2019-06-22 08:50:44'),
(34, 17, 97, 1, '2019-06-22 08:50:44', '2019-06-22 08:50:44'),
(35, 18, 52, 1, '2019-06-22 08:50:44', '2019-06-22 08:50:44'),
(36, 18, 21, 1, '2019-06-22 08:50:44', '2019-06-22 08:50:44'),
(37, 19, 48, 1, '2019-06-22 08:50:44', '2019-06-22 08:50:44'),
(38, 19, 76, 1, '2019-06-22 08:50:44', '2019-06-22 08:50:44'),
(39, 20, 21, 1, '2019-06-22 08:50:44', '2019-06-22 08:50:44'),
(40, 20, 11, 1, '2019-06-22 08:50:44', '2019-06-22 08:50:44'),
(41, 21, 5, 1, '2019-06-22 08:50:44', '2019-06-22 08:50:44'),
(42, 21, 48, 1, '2019-06-22 08:50:44', '2019-06-22 08:50:44'),
(43, 22, 47, 1, '2019-06-22 08:50:44', '2019-06-22 08:50:44'),
(44, 22, 38, 1, '2019-06-22 08:50:44', '2019-06-22 08:50:44'),
(45, 23, 60, 1, '2019-06-22 08:50:44', '2019-06-22 08:50:44'),
(46, 23, 76, 1, '2019-06-22 08:50:44', '2019-06-22 08:50:44'),
(47, 24, 16, 1, '2019-06-22 08:50:44', '2019-06-22 08:50:44'),
(48, 24, 33, 1, '2019-06-22 08:50:44', '2019-06-22 08:50:44'),
(49, 25, 10, 1, '2019-06-22 08:50:44', '2019-06-22 08:50:44'),
(50, 25, 91, 1, '2019-06-22 08:50:44', '2019-06-22 08:50:44'),
(51, 26, 18, 1, '2019-06-22 08:50:44', '2019-06-22 08:50:44'),
(52, 26, 27, 1, '2019-06-22 08:50:44', '2019-06-22 08:50:44'),
(53, 27, 16, 1, '2019-06-22 08:50:44', '2019-06-22 08:50:44'),
(54, 27, 8, 1, '2019-06-22 08:50:44', '2019-06-22 08:50:44'),
(55, 28, 69, 1, '2019-06-22 08:50:44', '2019-06-22 08:50:44'),
(56, 28, 86, 1, '2019-06-22 08:50:44', '2019-06-22 08:50:44'),
(57, 29, 67, 1, '2019-06-22 08:50:44', '2019-06-22 08:50:44'),
(58, 29, 25, 1, '2019-06-22 08:50:44', '2019-06-22 08:50:44'),
(59, 30, 35, 1, '2019-06-22 08:50:44', '2019-06-22 08:50:44'),
(60, 30, 74, 1, '2019-06-22 08:50:44', '2019-06-22 08:50:44'),
(61, 31, 3, 1, '2019-06-22 08:50:44', '2019-06-22 08:50:44'),
(62, 31, 94, 1, '2019-06-22 08:50:44', '2019-06-22 08:50:44'),
(63, 32, 79, 1, '2019-06-22 08:50:44', '2019-06-22 08:50:44'),
(64, 32, 64, 1, '2019-06-22 08:50:44', '2019-06-22 08:50:44'),
(65, 33, 54, 1, '2019-06-22 08:50:44', '2019-06-22 08:50:44'),
(66, 33, 25, 1, '2019-06-22 08:50:44', '2019-06-22 08:50:44'),
(67, 34, 85, 1, '2019-06-22 08:50:44', '2019-06-22 08:50:44'),
(68, 34, 44, 1, '2019-06-22 08:50:44', '2019-06-22 08:50:44'),
(69, 35, 82, 1, '2019-06-22 08:50:44', '2019-06-22 08:50:44'),
(70, 35, 27, 1, '2019-06-22 08:50:44', '2019-06-22 08:50:44'),
(71, 36, 30, 1, '2019-06-22 08:50:44', '2019-06-22 08:50:44'),
(72, 36, 82, 1, '2019-06-22 08:50:44', '2019-06-22 08:50:44'),
(73, 37, 74, 1, '2019-06-22 08:50:44', '2019-06-22 08:50:44'),
(74, 37, 87, 1, '2019-06-22 08:50:44', '2019-06-22 08:50:44'),
(75, 38, 74, 1, '2019-06-22 08:50:44', '2019-06-22 08:50:44'),
(76, 38, 42, 1, '2019-06-22 08:50:44', '2019-06-22 08:50:44'),
(77, 39, 61, 1, '2019-06-22 08:50:44', '2019-06-22 08:50:44'),
(78, 39, 38, 1, '2019-06-22 08:50:44', '2019-06-22 08:50:44'),
(79, 40, 66, 1, '2019-06-22 08:50:44', '2019-06-22 08:50:44'),
(80, 40, 24, 1, '2019-06-22 08:50:44', '2019-06-22 08:50:44'),
(81, 41, 26, 1, '2019-06-22 08:50:44', '2019-06-22 08:50:44'),
(82, 41, 48, 1, '2019-06-22 08:50:44', '2019-06-22 08:50:44'),
(83, 42, 55, 1, '2019-06-22 08:50:44', '2019-06-22 08:50:44'),
(84, 42, 58, 1, '2019-06-22 08:50:44', '2019-06-22 08:50:44'),
(85, 43, 7, 1, '2019-06-22 08:50:44', '2019-06-22 08:50:44'),
(86, 43, 67, 1, '2019-06-22 08:50:44', '2019-06-22 08:50:44'),
(87, 44, 45, 1, '2019-06-22 08:50:44', '2019-06-22 08:50:44'),
(88, 44, 63, 1, '2019-06-22 08:50:44', '2019-06-22 08:50:44'),
(89, 45, 89, 1, '2019-06-22 08:50:44', '2019-06-22 08:50:44'),
(90, 45, 74, 1, '2019-06-22 08:50:44', '2019-06-22 08:50:44'),
(91, 46, 43, 1, '2019-06-22 08:50:44', '2019-06-22 08:50:44'),
(92, 46, 9, 1, '2019-06-22 08:50:44', '2019-06-22 08:50:44'),
(93, 47, 32, 1, '2019-06-22 08:50:44', '2019-06-22 08:50:44'),
(94, 47, 67, 1, '2019-06-22 08:50:44', '2019-06-22 08:50:44'),
(95, 48, 82, 1, '2019-06-22 08:50:44', '2019-06-22 08:50:44'),
(96, 48, 97, 1, '2019-06-22 08:50:44', '2019-06-22 08:50:44'),
(97, 49, 62, 1, '2019-06-22 08:50:44', '2019-06-22 08:50:44'),
(98, 49, 31, 1, '2019-06-22 08:50:44', '2019-06-22 08:50:44'),
(99, 50, 26, 1, '2019-06-22 08:50:44', '2019-06-22 08:50:44'),
(100, 50, 12, 1, '2019-06-22 08:50:44', '2019-06-22 08:50:44');

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

DROP TABLE IF EXISTS `password_resets`;
CREATE TABLE IF NOT EXISTS `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `password_resets_email_index` (`email`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `permissions`
--

DROP TABLE IF EXISTS `permissions`;
CREATE TABLE IF NOT EXISTS `permissions` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `ability_id` int(10) UNSIGNED NOT NULL,
  `entity_id` int(10) UNSIGNED DEFAULT NULL,
  `entity_type` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `forbidden` tinyint(1) NOT NULL DEFAULT '0',
  `scope` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `permissions_entity_index` (`entity_id`,`entity_type`,`scope`),
  KEY `permissions_ability_id_index` (`ability_id`),
  KEY `permissions_scope_index` (`scope`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

DROP TABLE IF EXISTS `products`;
CREATE TABLE IF NOT EXISTS `products` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `price` decimal(5,2) NOT NULL,
  `in_stock` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=101 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `products`
--

INSERT INTO `products` (`id`, `name`, `price`, `in_stock`, `created_at`, `updated_at`) VALUES
(1, 'Product_1', '97.00', 'No', '2019-06-22 05:30:20', '2019-06-22 05:30:20'),
(2, 'Product_2', '9.00', 'Yes', '2019-06-22 05:30:20', '2019-06-22 05:30:20'),
(3, 'Product_3', '21.00', 'No', '2019-06-22 05:30:20', '2019-06-22 05:30:20'),
(4, 'Product_4', '46.00', 'Yes', '2019-06-22 05:30:20', '2019-06-22 05:30:20'),
(5, 'Product_5', '43.00', 'No', '2019-06-22 05:30:20', '2019-06-22 05:30:20'),
(6, 'Product_6', '29.00', 'No', '2019-06-22 05:30:20', '2019-06-22 05:30:20'),
(7, 'Product_7', '35.00', 'Yes', '2019-06-22 05:30:20', '2019-06-22 05:30:20'),
(8, 'Product_8', '99.00', 'No', '2019-06-22 05:30:20', '2019-06-22 05:30:20'),
(9, 'Product_9', '18.00', 'Yes', '2019-06-22 05:30:20', '2019-06-22 05:30:20'),
(10, 'Product_10', '88.00', 'Yes', '2019-06-22 05:30:20', '2019-06-22 05:30:20'),
(11, 'Product_11', '90.00', 'Yes', '2019-06-22 05:30:20', '2019-06-22 05:30:20'),
(12, 'Product_12', '78.00', 'Yes', '2019-06-22 05:30:20', '2019-06-22 05:30:20'),
(13, 'Product_13', '65.00', 'No', '2019-06-22 05:30:20', '2019-06-22 05:30:20'),
(14, 'Product_14', '45.00', 'No', '2019-06-22 05:30:20', '2019-06-22 05:30:20'),
(15, 'Product_15', '62.00', 'Yes', '2019-06-22 05:30:20', '2019-06-22 05:30:20'),
(16, 'Product_16', '62.00', 'No', '2019-06-22 05:30:20', '2019-06-22 05:30:20'),
(17, 'Product_17', '26.00', 'Yes', '2019-06-22 05:30:20', '2019-06-22 05:30:20'),
(18, 'Product_18', '3.00', 'No', '2019-06-22 05:30:20', '2019-06-22 05:30:20'),
(19, 'Product_19', '36.00', 'No', '2019-06-22 05:30:20', '2019-06-22 05:30:20'),
(20, 'Product_20', '77.00', 'No', '2019-06-22 05:30:20', '2019-06-22 05:30:20'),
(21, 'Product_21', '66.00', 'No', '2019-06-22 05:30:20', '2019-06-22 05:30:20'),
(22, 'Product_22', '55.00', 'Yes', '2019-06-22 05:30:20', '2019-06-22 05:30:20'),
(23, 'Product_23', '96.00', 'Yes', '2019-06-22 05:30:20', '2019-06-22 05:30:20'),
(24, 'Product_24', '61.00', 'No', '2019-06-22 05:30:20', '2019-06-22 05:30:20'),
(25, 'Product_25', '22.00', 'Yes', '2019-06-22 05:30:20', '2019-06-22 05:30:20'),
(26, 'Product_26', '59.00', 'No', '2019-06-22 05:30:20', '2019-06-22 05:30:20'),
(27, 'Product_27', '74.00', 'Yes', '2019-06-22 05:30:20', '2019-06-22 05:30:20'),
(28, 'Product_28', '56.00', 'No', '2019-06-22 05:30:20', '2019-06-22 05:30:20'),
(29, 'Product_29', '25.00', 'Yes', '2019-06-22 05:30:20', '2019-06-22 05:30:20'),
(30, 'Product_30', '48.00', 'Yes', '2019-06-22 05:30:20', '2019-06-22 05:30:20'),
(31, 'Product_31', '50.00', 'No', '2019-06-22 05:30:20', '2019-06-22 05:30:20'),
(32, 'Product_32', '72.00', 'No', '2019-06-22 05:30:20', '2019-06-22 05:30:20'),
(33, 'Product_33', '32.00', 'No', '2019-06-22 05:30:20', '2019-06-22 05:30:20'),
(34, 'Product_34', '47.00', 'Yes', '2019-06-22 05:30:20', '2019-06-22 05:30:20'),
(35, 'Product_35', '20.00', 'Yes', '2019-06-22 05:30:20', '2019-06-22 05:30:20'),
(36, 'Product_36', '48.00', 'No', '2019-06-22 05:30:20', '2019-06-22 05:30:20'),
(37, 'Product_37', '92.00', 'No', '2019-06-22 05:30:20', '2019-06-22 05:30:20'),
(38, 'Product_38', '12.00', 'No', '2019-06-22 05:30:20', '2019-06-22 05:30:20'),
(39, 'Product_39', '54.00', 'Yes', '2019-06-22 05:30:20', '2019-06-22 05:30:20'),
(40, 'Product_40', '52.00', 'Yes', '2019-06-22 05:30:20', '2019-06-22 05:30:20'),
(41, 'Product_41', '72.00', 'Yes', '2019-06-22 05:30:20', '2019-06-22 05:30:20'),
(42, 'Product_42', '68.00', 'Yes', '2019-06-22 05:30:20', '2019-06-22 05:30:20'),
(43, 'Product_43', '70.00', 'Yes', '2019-06-22 05:30:20', '2019-06-22 05:30:20'),
(44, 'Product_44', '6.00', 'No', '2019-06-22 05:30:20', '2019-06-22 05:30:20'),
(45, 'Product_45', '33.00', 'Yes', '2019-06-22 05:30:20', '2019-06-22 05:30:20'),
(46, 'Product_46', '31.00', 'Yes', '2019-06-22 05:30:20', '2019-06-22 05:30:20'),
(47, 'Product_47', '73.00', 'No', '2019-06-22 05:30:20', '2019-06-22 05:30:20'),
(48, 'Product_48', '54.00', 'Yes', '2019-06-22 05:30:20', '2019-06-22 05:30:20'),
(49, 'Product_49', '33.00', 'Yes', '2019-06-22 05:30:20', '2019-06-22 05:30:20'),
(50, 'Product_50', '34.00', 'No', '2019-06-22 05:30:20', '2019-06-22 05:30:20'),
(51, 'Product_51', '2.00', 'No', '2019-06-22 05:30:20', '2019-06-22 05:30:20'),
(52, 'Product_52', '71.00', 'Yes', '2019-06-22 05:30:20', '2019-06-22 05:30:20'),
(53, 'Product_53', '54.00', 'Yes', '2019-06-22 05:30:20', '2019-06-22 05:30:20'),
(54, 'Product_54', '14.00', 'No', '2019-06-22 05:30:20', '2019-06-22 05:30:20'),
(55, 'Product_55', '0.00', 'Yes', '2019-06-22 05:30:20', '2019-06-22 05:30:20'),
(56, 'Product_56', '26.00', 'No', '2019-06-22 05:30:20', '2019-06-22 05:30:20'),
(57, 'Product_57', '10.00', 'No', '2019-06-22 05:30:20', '2019-06-22 05:30:20'),
(58, 'Product_58', '74.00', 'Yes', '2019-06-22 05:30:20', '2019-06-22 05:30:20'),
(59, 'Product_59', '11.00', 'Yes', '2019-06-22 05:30:20', '2019-06-22 05:30:20'),
(60, 'Product_60', '34.00', 'No', '2019-06-22 05:30:20', '2019-06-22 05:30:20'),
(61, 'Product_61', '72.00', 'No', '2019-06-22 05:30:20', '2019-06-22 05:30:20'),
(62, 'Product_62', '59.00', 'No', '2019-06-22 05:30:20', '2019-06-22 05:30:20'),
(63, 'Product_63', '92.00', 'No', '2019-06-22 05:30:20', '2019-06-22 05:30:20'),
(64, 'Product_64', '17.00', 'Yes', '2019-06-22 05:30:20', '2019-06-22 05:30:20'),
(65, 'Product_65', '35.00', 'Yes', '2019-06-22 05:30:20', '2019-06-22 05:30:20'),
(66, 'Product_66', '6.00', 'Yes', '2019-06-22 05:30:20', '2019-06-22 05:30:20'),
(67, 'Product_67', '83.00', 'No', '2019-06-22 05:30:20', '2019-06-22 05:30:20'),
(68, 'Product_68', '9.00', 'Yes', '2019-06-22 05:30:20', '2019-06-22 05:30:20'),
(69, 'Product_69', '90.00', 'Yes', '2019-06-22 05:30:20', '2019-06-22 05:30:20'),
(70, 'Product_70', '20.00', 'No', '2019-06-22 05:30:20', '2019-06-22 05:30:20'),
(71, 'Product_71', '88.00', 'Yes', '2019-06-22 05:30:20', '2019-06-22 05:30:20'),
(72, 'Product_72', '14.00', 'Yes', '2019-06-22 05:30:20', '2019-06-22 05:30:20'),
(73, 'Product_73', '22.00', 'No', '2019-06-22 05:30:20', '2019-06-22 05:30:20'),
(74, 'Product_74', '56.00', 'No', '2019-06-22 05:30:20', '2019-06-22 05:30:20'),
(75, 'Product_75', '89.00', 'Yes', '2019-06-22 05:30:20', '2019-06-22 05:30:20'),
(76, 'Product_76', '56.00', 'No', '2019-06-22 05:30:20', '2019-06-22 05:30:20'),
(77, 'Product_77', '53.00', 'Yes', '2019-06-22 05:30:20', '2019-06-22 05:30:20'),
(78, 'Product_78', '35.00', 'Yes', '2019-06-22 05:30:20', '2019-06-22 05:30:20'),
(79, 'Product_79', '54.00', 'Yes', '2019-06-22 05:30:20', '2019-06-22 05:30:20'),
(80, 'Product_80', '31.00', 'Yes', '2019-06-22 05:30:20', '2019-06-22 05:30:20'),
(81, 'Product_81', '59.00', 'No', '2019-06-22 05:30:20', '2019-06-22 05:30:20'),
(82, 'Product_82', '44.00', 'No', '2019-06-22 05:30:20', '2019-06-22 05:30:20'),
(83, 'Product_83', '81.00', 'No', '2019-06-22 05:30:20', '2019-06-22 05:30:20'),
(84, 'Product_84', '87.00', 'Yes', '2019-06-22 05:30:20', '2019-06-22 05:30:20'),
(85, 'Product_85', '85.00', 'No', '2019-06-22 05:30:20', '2019-06-22 05:30:20'),
(86, 'Product_86', '44.00', 'No', '2019-06-22 05:30:20', '2019-06-22 05:30:20'),
(87, 'Product_87', '33.00', 'No', '2019-06-22 05:30:20', '2019-06-22 05:30:20'),
(88, 'Product_88', '77.00', 'No', '2019-06-22 05:30:20', '2019-06-22 05:30:20'),
(89, 'Product_89', '67.00', 'No', '2019-06-22 05:30:20', '2019-06-22 05:30:20'),
(90, 'Product_90', '43.00', 'No', '2019-06-22 05:30:20', '2019-06-22 05:30:20'),
(91, 'Product_91', '68.00', 'No', '2019-06-22 05:30:20', '2019-06-22 05:30:20'),
(92, 'Product_92', '87.00', 'Yes', '2019-06-22 05:30:20', '2019-06-22 05:30:20'),
(93, 'Product_93', '32.00', 'Yes', '2019-06-22 05:30:20', '2019-06-22 05:30:20'),
(94, 'Product_94', '99.00', 'Yes', '2019-06-22 05:30:20', '2019-06-22 05:30:20'),
(95, 'Product_95', '99.00', 'Yes', '2019-06-22 05:30:20', '2019-06-22 05:30:20'),
(96, 'Product_96', '54.00', 'Yes', '2019-06-22 05:30:20', '2019-06-22 05:30:20'),
(97, 'Product_97', '64.00', 'No', '2019-06-22 05:30:20', '2019-06-22 05:30:20'),
(98, 'Product_98', '97.00', 'No', '2019-06-22 05:30:20', '2019-06-22 05:30:20'),
(99, 'Product_99', '18.00', 'No', '2019-06-22 05:30:20', '2019-06-22 05:30:20'),
(100, 'Product_100', '92.00', 'Yes', '2019-06-22 05:30:20', '2019-06-22 05:30:20');

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

DROP TABLE IF EXISTS `roles`;
CREATE TABLE IF NOT EXISTS `roles` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `level` int(10) UNSIGNED DEFAULT NULL,
  `scope` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `roles_name_unique` (`name`,`scope`),
  KEY `roles_scope_index` (`scope`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `roles`
--

INSERT INTO `roles` (`id`, `name`, `title`, `level`, `scope`, `created_at`, `updated_at`) VALUES
(1, 'admin', 'Admin', NULL, NULL, '2019-06-22 06:50:31', '2019-06-22 06:50:31'),
(2, 'user', 'User', NULL, NULL, '2019-06-22 06:53:16', '2019-06-22 06:53:16'),
(3, 'shop', 'Shop', NULL, NULL, '2019-06-22 06:54:21', '2019-06-22 06:54:21');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
CREATE TABLE IF NOT EXISTS `users` (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `email_verified_at`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'administrator', 'administrator@gmail.com', NULL, '$2y$10$sgH3VVixqdMXv9z1/mNY7uUjoPv0txv87VLgBQvGYTxh642PSS3Ya', NULL, '2019-06-22 05:28:09', '2019-06-22 05:28:09'),
(2, 'user-manager', 'user-manager@gmail.com', NULL, '$2y$10$b.qsj6OT28/xMpNBOf3Tf.Oqo8sHwJavzh63gJ0wR6PSRFJYE13N2', NULL, '2019-06-22 05:28:38', '2019-06-22 05:28:38'),
(3, 'shop-manager', 'shop-manager@gmail.com', NULL, '$2y$10$gG6Ni54ln9fJlnA8XwDrpuebFH36/3x1u1BB3.HdubmNFSeh1IlF.', NULL, '2019-06-22 05:29:03', '2019-06-22 05:29:03');
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
