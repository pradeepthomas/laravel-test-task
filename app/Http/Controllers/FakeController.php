<?php
/*
Author: Pradeep
Date: 21/06/2019
Version: 1.0
Description: Controller for faker to create fake data
*/
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Faker\Factory;
use DB;
class FakeController extends Controller
{
    public function index(){
		
	}
    public function products(){
		$faker = Factory::create();
		for ($i = 0; $i < 100; $i++) {		  	  
			DB::table('products')->insert([
				'name' => 'Product_'.($i+1),
				'price' => $faker->randomNumber(2),				
				'in_stock' => 'Yes',				
				'created_at' => date('Y-m-d H:i:s'),
				'updated_at' => date('Y-m-d H:i:s')
			]);
		}		
		$get=DB::table('products')->orderBy(DB::raw('RAND()'))->take(50)->get();
		foreach($get as $row){
			DB::table('products')->where('id',$row->id)->update(['in_stock' => 'No']);			
		}
    }
	public function customers(){
		$faker = Factory::create();
		for ($i = 0; $i < 200; $i++) {		  	  
			DB::table('customers')->insert([
				'name' => $faker->name,
				'email' => $faker->email,				
				'created_at' => date('Y-m-d H:i:s'),
				'updated_at' => date('Y-m-d H:i:s')
			]);
		}
    }
	public function order(){
		$faker = Factory::create();
		for ($i = 0; $i < 50; $i++) {		  	  
			$customers=DB::table('customers')->orderBy(DB::raw('RAND()'))->take(1)->get()[0];
			$insert=DB::table('orders')->insert([
				'invoice_number' => '000'.($i+1),
				'total_amount' => $faker->randomNumber(2),				
				'status' => 'new',				
				'customer_id' => $customers->id,
				'created_at' => date('Y-m-d H:i:s'),
				'updated_at' => date('Y-m-d H:i:s')
			]);
			$order_id = DB::getPdo()->lastInsertId();
			
			$total_amount=0;
			$product=DB::table('products')->orderBy(DB::raw('RAND()'))->take(2)->get();
			foreach($product as $row){
				$insert=DB::table('order_items')->insert([
					'order_id' => $order_id,
					'product_id' => $row->id,
					'quantity' => 1,
					'created_at' => date('Y-m-d H:i:s'),
					'updated_at' => date('Y-m-d H:i:s')
				]);
				$total_amount=$row->price+$total_amount;
			}
			DB::table('orders')->where('id',$order_id)->update(['total_amount' => $total_amount]);
		}
		
		$get=DB::table('orders')->orderBy(DB::raw('RAND()'))->take(25)->get();
		foreach($get as $row){
			DB::table('orders')->where('id',$row->id)->update(['status' => 'processed']);			
		}
	}
}
