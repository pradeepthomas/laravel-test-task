<?php
/*
Author: Pradeep
Date: 21/06/2019
Version: 1.0
Description: Customer controller to get CustomerList
*/
namespace App\Http\Controllers;
use App\Customer;
use Illuminate\Http\Request;
use Mail;
use Validator;
use DB;
use App\Helpers\Helper;	
Use Auth;
class CustomerController extends Controller
{
	public function customer(){
		$role=Auth::user()->roles()->pluck('name')[0];
		if($role=='admin'){
			return view('customerview');
		}else{
			return abort(404);
		}
	}
	public function datatables(){			
		return datatables(Customer::all())->toJson();
	}	
}
?>		