<?php
/*
Author: Pradeep
Date: 21/06/2019
Version: 1.0
Description: order controller to get orderList and order Details.
*/
namespace App\Http\Controllers;
use App\Order;
use App\Customer;
use App\Product;
use App\Order_item;
use Illuminate\Http\Request;
use Mail;
use Validator;
use DB;
use App\Helpers\Helper;	
Use Auth;
class OrderController extends Controller
{
	public function order(){
		$role=Auth::user()->roles()->pluck('name')[0];
		if($role=='admin' || $role=='shop'){
			return view('orderview');
		}else{
			return abort(404);
		}
	}
	public function datatables(){			    
		$order_details=Customer::join('orders', 'customers.id', '=', 'orders.customer_id')->get();			
		return datatables($order_details)->toJson();
	}
	public function getOrderDetails($id){    			
		$order_items=Order::join('order_items', 'order_items.order_id', '=', 'orders.id')
			->join('products', 'products.id', '=', 'order_items.product_id')
			->join('customers', 'customers.id', '=', 'orders.customer_id')
			->where('orders.id', $id)
			->select('products.id as products_id',
					'products.name as products_name',
					'customers.name as customer_name',
					'customers.email as customer_email',
					'customers.id as customer_id',
					'orders.id as orders_id',						
					'orders.*',
					'products.*')
			->get();			
		$data["order_details"]=$order_items;			
		activity()->log(Auth::user()->name .' processed the order: '.$id);
		return view("orderdetailview",$data);
	}				
}
?>		