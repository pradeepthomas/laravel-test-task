<?php
/*
Author: Pradeep
Date: 21/06/2019
Version: 1.0
Description: Product controller to get ProductList
*/
namespace App\Http\Controllers;
use App\Product;
use Illuminate\Http\Request;
use Mail;
use Validator;
use DB;
use App\Helpers\Helper;	
Use Auth;
class ProductController extends Controller
{
	public function product(){ // This function Used for load product page
		$role=Auth::user()->roles()->pluck('name')[0];
		if($role=='admin' || $role=='shop'){
			return view('productview');
		}else{
			return abort(404);
		}
	}
	public function datatables($id){	
		if($id==1){
			return datatables(Product::all())->toJson();
		}else if($id==2){
			return datatables(Product::where('in_stock', 'Yes')->get())->toJson();
		}else if($id==3){
			return datatables(Product::where('in_stock', 'No')->get())->toJson();
		}else{
			return datatables(Product::all())->toJson();
		}
	}	
}
?>		