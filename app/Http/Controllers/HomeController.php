<?php
/*
Author: Pradeep
Date: 21/06/2019
Version: 1.0
Description: Main controller to assign roles using Bouncer after login.
*/
namespace App\Http\Controllers;

use Illuminate\Http\Request;
Use Auth;
use Bouncer;
class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index(){        
		/* $user=Auth::user();
		$b=Bouncer::assign('shop')->to($user); */		
		return view('home');
    }
}
