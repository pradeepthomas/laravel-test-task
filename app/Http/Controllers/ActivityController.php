<?php
/*
Author: Pradeep
Date: 21/06/2019
Version: 1.0
Description: Activity controller to get Activity Log 
*/
namespace App\Http\Controllers;
use App\Activity;
use Illuminate\Http\Request;
use Mail;
use Validator;
use DB;
use App\Helpers\Helper;	
Use Auth;
class ActivityController extends Controller
{
	public function activity(){ // This function Used for load product page
		$role=Auth::user()->roles()->pluck('name')[0];
		if($role=='admin'){
			return view('activityview');
		}else{
			return abort(404);
		}
	}
	public function datatables(){	
		return datatables(Activity::all())->toJson();
	}	
	}
?>		