<?php
/*
Author: Pradeep
Date: 21/06/2019
Version: 1.0
Description: Customer controller to get CustomerList
*/
namespace App\Http\Controllers;
use App\User;
use Illuminate\Http\Request;
use Mail;
use Validator;
use DB;
use App\Helpers\Helper;	
Use Auth;
class UserController extends Controller
{
	public function user(){
		$role=Auth::user()->roles()->pluck('name')[0];
		if($role=='admin' || $role=='user'){
			return view('userview');
		}else{
			return abort(404);
		}
	}
	public function datatables(){			
		return datatables(User::all())->toJson();
	}	
}
?>		